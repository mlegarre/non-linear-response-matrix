import time
from pathlib import Path
from response_matrix import ResponseMatrix

if __name__ == "__main__":
    KCD = {1:
               {'KCD.A12B1': -1000,
                'KCD.A23B1': -1000,
                'KCD.A34B1': -1000,
                'KCD.A45B1': -1000,
                'KCD.A56B1': -1000,
                'KCD.A67B1': -1000,
                'KCD.A78B1': -1000,
                'KCD.A81B1': -1000,
                },
           2:
               {'KCD.A12B2': -1000,
                'KCD.A23B2': -1000,
                'KCD.A34B2': -1000,
                'KCD.A45B2': -1000,
                'KCD.A56B2': -1000,
                'KCD.A67B2': -1000,
                'KCD.A78B2': -1000,
                'KCD.A81B2': -1000,
                }
           }
    # Define the base path were all the previously computed values are
    simulation = Path('8_families_MCD_at_once')

    # Define the beam
    beam = 1

    # Create the basic response matrix object
    resp = ResponseMatrix(KCD[beam], simulation, beam=beam)

    # Add the observables
    # Add the RDT to the response matrix
    measurement_path = Path(f'/afs/cern.ch/work/m/mlegarre/public/jupyter/resonance_driving_terms/measurements/2022-04-24/Analysis/Optics/OMC3/MCDO')
    model_path = Path(
        f'/afs/cern.ch/work/m/mlegarre/public/jupyter/resonance_driving_terms/simulations/response_matrix_check_b5'
        f'/Model/{beam}/twiss.dat')
    resp.add_rdt_observable(measurement_path, model_path, "f1004_x")

    # Add the Chromaticity to the response matrix
    chroma_path = Path('/afs/cern.ch/work/m/mlegarre/public/jupyter/chromaticity/measurements/2022-04-24_3')
    #resp.add_chromaticity_observable(chroma_path, order=3, weight=500)

    resp.add_zero_chromaticity_observable(order=3, weight=500)

    # Get the corrections
    corrections = resp.get_corrections(rcond=0.01,
                                       clean_nan=True,
                                       clean_outliers=True,
                                       clean_IR=False)
    time.sleep(0.01)
    print("Corrections before cleaning:")
    for key, val in corrections.items():
        print(f"  {key} = {val:6d} ;")

    # Get the same corrections but with some cleaning
    corrections_clean = resp.get_corrections(rcond=0.01,
                                             clean_nan=True,
                                             clean_outliers=True,
                                             clean_IR=True,
                                             inside_arc_number=10,
                                             quartiles=[25, 75, 1.5])
    time.sleep(0.01)
    print("Corrections after cleaning:")
    for key, val in corrections_clean.items():
        print(f"  {key} = {val:6d} ;")

    resp.plot_rdt_with_cleaning("real", 'f1004_x', ylim=(-10e6, 10e6), output='./output/real.png')
